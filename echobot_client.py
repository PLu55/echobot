import bluetooth as bt
import ctypes
import tempfile
import time
import os
import sys
import json
import platform

__version__ = "2.0.3"

python_major_version = int(platform.python_version().split('.')[0])

port = 1
robot_names = ['echo1', 'echo2']
btaddr_file = 'btaddr.json'
    
CMD_STOP = 1
CMD_MOVE = 2
CMD_MOVETO = 3
CMD_ROTATE = 4
CMD_ROTATETO = 5
CMD_GETSTATUS = 6
CMD_SHUTDOWN = 7
CMD_HSPEED = 8
CMD_VSPEED = 9
CMD_HACCEL = 10
CMD_VACCEL = 11
CMD_RESET = 12

STAT_OK = 0
STAT_REPLY = 1
STAT_CORRUPTED = 100
STAT_TIMEOUT = 101
STAT_UNKNOWN = 999

class EchoBotBTCliError(Exception):
    def __init__(self, code):
        self.code = code

class EchoBotBTCli(object):
    """Class that controlls the Echobot using Bluetooth to communicate with the
    hardware.
    """ 
    
    def __init__(self, port=1, robot_names=robot_names,
                 simulate=False, blocking=True, verbose=False):
        """Intitalizing the object.
        
        kwarg:
           robot_names: a string with the name of a single robot or a list of names of 
                        the robots, it is the name of the bluetooth service 
                        that it will be connected to. 
           port: is the number of the bluetooth port to use.
           simulate: if True it will not connect to anything but it turns verbose mode on
                     so each command will be printed on screen.
           blocking: if the commands will block until they are finished, default is True.
           verbose: if True informational text will be printed.
        """
        self.port = port
        self.simulate = simulate
        self.blocking = blocking
        self.verbose = verbose
        if simulate: self.verbose = True
        self.robot_names = robot_names
        if python_major_version == 2:
            if isinstance(self.robot_names, str) or isinstance(self.robot_names, unicode):
                self.robot_names = [self.robot_names]
        else:
            if isinstance(self.robot_names, str):
                self.robot_names = [self.robot_names]            
        self.devices = None
        if not simulate: self.connect()
            
    def _discover(self):
        bdev = bt.discover_devices(lookup_names=True)
        devices = {}
        for btaddr, name in bdev:
            if name in robot_names:
                devices[name] = btaddr
        with open(btaddr_file,'w') as f:
            json.dump(devices, f)
        return devices
    
    def connect(self, force = False):
        """Connect to all robots given when instantiated.
        """
        has_discovered = False
        if not force:
            if os.path.exists(btaddr_file):
                with open(btaddr_file,'r') as f:
                    self.devices = json.load(f)
        self.socks = []
        if self.devices is None:
            self.devices = self._discover()
            has_discovered = True
        for i, name in enumerate(self.devices.iterkeys()):
            print('Try to connect to {}...'.format(name))
            self.socks.append(bt.BluetoothSocket( bt.RFCOMM ))
            try:
                self.socks[i].connect((self.devices[name], port))
                print('Connected to: {}'.format(name))
            except IOError as e:
                if not has_discovered:
                    print('Try to rediscover bt devices...')
                    self.devices = self._discover()
                    has_discovered = True
                    try:
                        self.socks[i].connect((self.devices[name], port))
                        print('Connected to: {}'.format(name))
                    except IOError as e:
                        self.socks[i] = None
                        print('Can not connect to: {}!'.format(name))
                        print(e)
                else:
                    print('Can not connect to: {}!'.format(name))
                    print(e)
            if self.socks[i]:
                self.socks[i].setblocking(self.blocking)
    
    def close(self):
        """Close all the current connections.
        """
        for i in range(len(self.socks)):
            if not self.socks[i] is None:
                self.socks[i].close()
                self.socks[i] = None

    def _encodeCmd(self, command, val):
        val = int(round(val))
        buf = bytearray(6)
        buf[0] = command
        buf[1] = val >> 24 & 0xff
        buf[2] = val >> 16 & 0xff
        buf[3] = val >> 8 & 0xff
        buf[4] = val & 0xff
        csum = 0
        for b in buf[:-1]:
            csum += b
        buf[5] = csum & 0xff
        return bytes(buf)
    
    def _getReply(self, bot=0):
        buf = self.socks[bot].recv(6)
        self.socks[bot].settimeout(5)
        try:
            buf = bytearray(buf)
            csum = 0
            for b in buf[:-1]:
                csum += b
            check = buf[5] == (csum & 0xff)
            code = buf[0]
            val = ctypes.c_int32((buf[1] << 24) + (buf[2] << 16) +
                                 (buf[3] << 8) + buf[4]).value
        except socket.error:
            check = 0
            code = STAT_TIMEOUT
            val = 0
        return check, code, val

    def sendCmd(self, command, val, bot=0):
        """Send a command to the robot. (should not be used directly).

        Args:
           command: the command code.
           val: value of the command.
           bot: the number of the robot to send to, numbering start from 0.
        """
        
        buf = self._encodeCmd(command, val)
        self.socks[bot].send(buf)
        return self._getReply(bot)

    def _getStatus(self, bot=0):
        check, stat, val= self.sendCmd(CMD_GETSTATUS, 0, bot)
        if not check or stat != STAT_REPLY:
            raise EchoBotBTCliError(stat)
        return val

    def stop(self, bot=0):
        """Stops all ongoing movements in the robot.
        
        Args:
           bot: the number of the robot to stop, numbering start from 0.
        """
        if self.verbose:
            print('EchoBotBTCli: stop')
        if not self.simulate:
            check, stat, _ = self.sendCmd(CMD_STOP, 0, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)
        
    def reset(self, bot=0):
        """Set the reference position to the current position
        of both the horizontal position as well as the rotation.
        
        Args:
           bot: the number of the robot to send to, numbering start from 0.
        """
        
        if self.verbose:
            print('EchoBotBTCli: reset ')
        if not self.simulate:
            check, stat, _ = self.sendCmd(CMD_RESET, distance, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)

    def move(self, distance, bot=0):
        """Move the robot horizontally a given distance.
        
        Args:
           distance: the relative distance to move i meters.
           bot: the number of the robot to send to, numbering start from 0.
        """
        
        if self.verbose:
            print('EchoBotBTCli: move %f' %  distance)
        if not self.simulate:
            check, stat, _ = self.sendCmd(CMD_MOVE, int(round(distance*-1000)), bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)
        
    def moveTo(self, position, bot=0):
        """Move the robot to an absolute position.
        
        Args:
           position: the position to move to relative the reference point
                     (the position where the robot was initialized).
           bot: the number of the robot to send to, numbering start from 0.
        """
        
        if self.verbose:
            print('EchoBotBTCli: moveTo %f' %  position)
        if not self.simulate:
            check, stat, _ = self.sendCmd(CMD_MOVETO, int(round(position*-1000)) , bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)

    def rotate(self, angle, bot=0):
        """Rotate the target screen.
        
        Args: 
            angle : the angle to rotate the target screen in clockwise degrees.
            bot: the number of the robot to send to, numbering start from 0.
        """
        
        if self.verbose:
            print('EchoBotBTCli: rotate %f' %  angle)
        if not self.simulate:
            check, stat, _ = self.sendCmd(CMD_ROTATE, angle, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)

    def rotateTo(self, angle, bot=0):
        """Rotate the target screen to an absolute angle.
        
        Args: 
            angle: the angle to rotate the target screen to in clockwise degrees
                   from the reference angle (the angle of the target when the
                   robot was initialized).
            bot: the number of the robot to send to, numbering start from 0. 
        """
        if self.verbose:
            print('EchoBotBTCli: rotateTo %f' %  angle)
        if not self.simulate:
            check, stat, _= self.sendCmd(CMD_ROTATETO, angle, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)

    def waitUntilStop(self, bot='all'):
        """Wait until all movements of the robot of robots has stopped.
        
        Args:
            bot: the number of the robot to wait for, numbering start from 0.
                 if 'all' is given, it wait for all robots. Default is to wait
                 for all robots that are connected.
        """
        if self.verbose:
            print('EchoBotBTCli: waitUntilStop...')
        if not self.simulate:
            if isinstance(bot, int):
                bot = [bot]
            elif bot == 'all':
                bot = range(len(self.socks))
            for i in bot:
                while self._getStatus(i) != 0:
                    time.sleep(0.2)
        else:
            time.sleep(1.0)
        if self.verbose:
            print('   ... done waiting!')

    def setHSpeed(self, vel, bot=0):
        """Set the maximum speed for the horizontal motor. This is the speed 
        that the motor has after the acceleration is finished.
        
        Args:
             vel: is the speed [0-2047], see StepRocker manual for more information.
             bot: the number of the robot to send to, numbering start from 0.
        """
        if self.verbose:
            print('EchoBotBTCli: setHSpeed %f' %  vel)
        if not self.simulate:
            check, stat, _= self.sendCmd(CMD_HSPEED, vel, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)
        
    def setVSpeed(self, vel, bot=0):
        """Set the maximum speed for the vertical motor. This is the speed
        that the motor has after the acceleration is finished.
        
        Args:
            vel: is the speed [0-2047], see StepRocker manual for more information.
            bot: the number of the robot to send to, numbering start from 0.
        """
        if self.verbose:
            print('EchoBotBTCli: setVSpeed %f' %  vel)
        if not self.simulate:
            check, stat, _= self.sendCmd(CMD_VSPEED, vel, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)


    def setHAcceleration(self, acc, bot=0):
        """Set the maximum accelaration for the horizontal motor.
        
        Args:
            acc: is the acceleration [0-2047], see StepRocker manual for more
                 information.
            bot: the number of the robot to send to, numbering start from 0.
        """
        if self.verbose:
            print('EchoBotBTCli: setHAcceleration %f' %  acc)
        if not self.simulate:
            check, stat, _= self.sendCmd(CMD_HACCEL, acc, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)

    def setVAcceleration(self, acc, bot=0):
        """Set the maximum accelaration for the vertical motor.
        
        Args:
            acc: is the acceleration [0-2047], see StepRocker manual for more
                 information.
            bot: the number of the robot to send to, numbering start from 0.
        """
        if self.verbose:
            print('EchoBotBTCli: setVAcceleration %f' %  acc)
        if not self.simulate:
            check, stat, _= self.sendCmd(CMD_VACCEL, acc, bot)
            if not check or stat != STAT_OK:
                raise EchoBotBTCliError(stat)
        

if __name__ == '__main__':
    
    eb = EchoBotBTCli(simulate=True)
    eb.move(13.1)
    eb.moveTo(7.14)
    eb.rotate(45.0)
    eb.rotateTo(-37.0)
    eb.waitUntilStop()
    
